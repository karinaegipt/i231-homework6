import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
   Ülesanne: Olgu antud geograafiliste punktide graaf, mille iga tipuga
   on seotud väli .h – vastava punkti kõrgus merepinnast. Kirjutada graafi
   laiuti läbimisel põhinev algoritm, mille käigus leitakse selline tee antud
   punktist a antud punkti b, mis läheb läbi võimalikult kõrge punkti.

   @author Jaanus Pöial (tooriku autor)
   @author Karina Egipt, DK22
   28.04.2016
*/



/** Põhiklass programmi silumiseks*/
public class GraphTask {

   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.test1();
   }

   /**
    * Esialgne meetod graafi loomiseks. reateRandomSimpleGraph-ile antakse kaasa
    * tippude ja kaarte arv
    */
   public void run() {
      Graph g = new Graph("G");
      g.createRandomSimpleGraph(6, 9);
      System.out.println(g);
   }

   /**
    * Meetod programmi tetsimiseks. Loome tavalise graafi andes kaasa tipud ja kaared.
    * On võimalik leida erinevaid teid tippude vahel.
    */
   public void test1() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 5);
      System. out.println (g);
      Vertex a = g.first;
      System. out.println("a: " + a);
      Vertex b = g.first.next;
      System. out.println("b: " + b);
      System. out.println();
      String road = g.findWayThroughHighestPoint(a, b).pathToString();
      System.out.println(road);
   }

   /**
    * Meetod programmi testimiseks juhul, kui graafi kõik tipud
    * on sama kõrgusega
    */
   public void test2() {
      Graph g = new Graph ("G");
      Vertex v1 = new Vertex("v1", null, null, 66, Color.None);
      Vertex v2 = new Vertex("v2", null, null, 66, Color.None);
      Vertex v3 = new Vertex("v3", null, null, 66, Color.None);
      Vertex v4 = new Vertex("v4", null, null, 66, Color.None);
      Vertex v5 = new Vertex("v5", null, null, 66, Color.None);
      g.setFirstVertex(v1);
      v1.setNext(v2);
      v2.setNext(v3);
      v3.setNext(v4);
      v4.setNext(v5);

      Arc e1 = new Arc("e1v1_v2", v2, null);
      v1.setFirstArc(e1);
      Arc e2 = new Arc("e2v1_v3", v3, null);
      e1.setNext(e2);
      Arc e3 = new Arc("e3v1_v4", v3, null);
      e2.setNext(e3);
      Arc e4 = new Arc("e4v2_v5", v5, null);
      v2.setFirstArc(e4);
      Arc e5 = new Arc("e5v3_v4", v4, null);
      v3.setFirstArc(e5);
      Arc e6 = new Arc("e6v5_v1", v1, null);
      v5.setFirstArc(e6);

      System. out.println(g.toString());
      System. out.println("a: " + v2.toString());
      System. out.println("b: " + v4.toString());
      String road = g.findWayThroughHighestPoint(v2, v4).pathToString();
      System. out.println(road);
   }


   /** Meetod programmi testimiseks juhul, kui graafi tippude vahel
    * ei ole ühtegi seost.*/
   public void test3() {
      Graph g = new Graph ("G");
      Vertex v1 = new Vertex("v1", null, null, 1, Color.None);
      Vertex v2 = new Vertex("v2", null, null, 2, Color.None);
      Vertex v3 = new Vertex("v3", null, null, 3, Color.None);
      Vertex v4 = new Vertex("v4", null, null, 4, Color.None);
      Vertex v5 = new Vertex("v5", null, null, 5, Color.None);
      g.setFirstVertex(v1);
      v1.setNext(v2);
      v2.setNext(v3);
      v3.setNext(v4);
      v4.setNext(v5);
      System. out.println(g.toString());
      System. out.println("a: " + v2.toString());
      System. out.println("b: " + v4.toString());
      String road = g.findWayThroughHighestPoint(v2, v5).pathToString();
      System. out.println(road);
   }
   /** Meetod programmi testimiseks*/
   public void test4() {
      Graph g = new Graph ("G");
      Vertex v1 = new Vertex("v1", null, null, 45, Color.None);
      Vertex v2 = new Vertex("v2", null, null, 7, Color.None);
      Vertex v3 = new Vertex("v3", null, null, 99, Color.None);
      Vertex v4 = new Vertex("v4", null, null, 76, Color.None);
      Vertex v5 = new Vertex("v5", null, null, 32, Color.None);
      Vertex v6 = new Vertex("v5", null, null, 32, Color.None);
      g.setFirstVertex(v1);
      v1.setNext(v2);
      v2.setNext(v3);
      v3.setNext(v4);
      v4.setNext(v5);
      v5.setNext(v6);

      Arc e1 = new Arc("e1v1_v2", v2, null);
      v1.setFirstArc(e1);
      Arc e2 = new Arc("e2v1_v2", v2, null);
      e1.setNext(e2);
      Arc e3 = new Arc("e3v1_v4", v3, null);
      e2.setNext(e3);
      Arc e4 = new Arc("e4v2_v5", v5, null);
      v2.setFirstArc(e4);
      Arc e5 = new Arc("e5v3_v4", v4, null);
      v3.setFirstArc(e5);
      Arc e6 = new Arc("e6v5_v1", v1, null);
      v5.setFirstArc(e6);

      System. out.println(g.toString());
      System. out.println("a: " + v2.toString());
      System. out.println("b: " + v6.toString());
      String road = g.findWayThroughHighestPoint(v2, v6).pathToString();
      System. out.println(road);
   }

   /** Enum graafi läbimisel punktide staatuse võimalike väärtusega */
    public enum Color {
      White, Grey, Black, None
   }


   /**
    * Klass graafi tippude loomiseks ja manipuleerimiseks.
    * @author Jaanus Pöial (tooriku autor)
    * @author Karina Egipt
    */
   public class Vertex {
      /** Tipu nimi*/
      private String id;
      /**Järgmine tipp, kuhu suundutakse*/
      private Vertex next;
      /**Viide esimesele tipust väljuvale kaarele*/
      private Arc first;
      /**Infoväli*/
      private int info = 0;
      /**Punkti kõrgus merepinnast*/
      private int h = 0;
      /**Punkti staatus graafi läbimisel*/
      public Color state;

      /**
       * Konstruktor Vertex-objekti loomiseks
       * Kaasa antakse järgmised parameetrid
       * @param s loodava tipu nimi
       * @param v järgmine tipp
       * @param e järgmisesse tippu suunduv kaar
       * @param h tipu kõrgus
       * @param state tipu staatus (värv) graafi töötlemisel
       */
      Vertex (String s, Vertex v, Arc e, int h, Color state) {
         id = s;
         next = v;
         first = e;
         this.h = h;
         this.state = state;
      }
      /**
       * Teine konstruktor Vertex-objekti loomiseks
       * Kaasa antakse järgmised parameetrid
       * @param s loodava tipu nimi
       * @param h tipu kõrgus
       * Järgmise tipu, väljuva kaare väärtusteks pannakse vaikimisi null.
       * Tipu värvuseks märgitakse vaikimisi None.
       */
      Vertex (String s, int h) {
         this (s, null, null, h, Color.None);
      }

      /**Meetod tipu nime ja kõrguse tagastamiseks stringina
       * @return tipu nimi ja kõrgus stingina */
      @Override
      public String toString() {
         return id + "(" + h + ")";
      }

      /**Meetod tipu värvi määramiseks
       * @param state tipu värvi väärtus enumist Color*/
      public void setVColor (Color state) {
         this.state = state;
      }

      /**Meetod tipu värvi küsimiseks
       * @return tipu värvus */
      public Color getVColor () {
         return state;
      }

      /** Meetod tipus väljuva kaare küsimiseks       *
       * @return järgmisesse tippu väljuv kaar Arrc tüüpi objektina
        */
      public Arc getFirstArc() {
         return first;
      }

      /**Meetod järgmise tipu määramiseks       *
       * @param next järgmine tipp Vertex-tüüpi objektina
        */
      public void setNext(Vertex next) {
         this.next = next;
      }

      /**
       * Meetod tipust väljuva kaare loomiseks
       * @param first Tipust väljuv kaar Arc-tüüpi objektina
        */
      public void setFirstArc(Arc first) {
         this.first = first;
      }
   } /*End Vertex*/


   /**
    * Klass graafi tippudest väljuvate kaarte loomiseks ja manipuleerimiseks.
    * @author Jaanus Pöial (tooriku autor)
    * @author Karina Egipt
    */
   class Arc {
      /** Kaare nimi */
      private String id;
      /** Punkt kuhu kaar suundub */
      private Vertex target;
      /** Samast punktist väljuv järgmine kaar */
      private Arc next;
      /** Infos */
      private int info = 0;

      /** Konstruktor kaare loomiseks       *
       * @param s kaare nimi stringina
       * @param v punkt kuhu kaar suundub Vertex-tüüpi objektina
       * @param a samast punktist väljuv järgmine kaar Arc-tüüpi objektina
        */
      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }
      /** Teine konstruktor kaare loomiseks       *
       * @param s kaare nimi stringina
       * Vaikimisi määratakse siht-punktiks ja järgmiseks kaareks null
       */
      Arc (String s) {
         this (s, null, null);
      }

      /**
       * Meetod kaare nime tagastamiseks
       * @return kaare nimi stringina
        */
      @Override
      public String toString() {
         return id;
      }

      public String pathToString() {
         List<String> pathArcs = new ArrayList<String>();
         Arc current = this;
         do {
            pathArcs.add(current.toString());
            current = current.next;
         } while (current != null);
         return StringUtils.join(pathArcs, " -> ");
      }

       /**
        * Meetod samast punktist väljuva järgmise kaare määramiseks
        * @param next   samast punktist väljuv kaar Arc-tüüpi objektina
        */
      public void setNext(Arc next) {
         this.next = next;
      }
   } /*End Arc*/


   /**
    * Klass graafi loomiseks ja manipuleerimiseks.
    * @author Jaanus Pöial (tooriku autor)
    * @author Karina Egipt
    */
   class Graph {
      /**Graafi nimi*/
      private String id;
      /**Graafi esimene punkt*/
      private Vertex first;
      /**Info*/
      private int info = 0;

       /**
        * Konstruktor graafi loomiseks
        * @param s graafi nimi stringina
        * @param v graafi esimene punkti Vertex-tüüpi objektina
        */
      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      /**
       * Teine konstruktor graafi loomiseks
       * @param s graafi nimi stringina
       * Vaikimisi määratakse esimeseks punktiks null
        */
      Graph (String s) {
         this (s, null);
      }

      /**
       * Meetod graafi väljakirjutamiseks. Käib läbi kogu graafi
       * @return graaf stringina. Eraldi tuuakse välja nii tipud kui ka neid
       *          ühendavad kaared, st kogu külgnevusstruktuur
        */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
               while (a != null) {
                  sb.append (" ");
                  sb.append (a.toString());
                  sb.append (" (");
                  sb.append (v.toString());
                  sb.append ("->");
                  sb.append (a.target.toString());
                  sb.append (")");
                  a = a.next;
               }
               sb.append (nl);
               v = v.next;
            }
            return sb.toString();
      }

      /**
       *Meetod tipu loomiseks, mis ühtlasi määratakse graafi esimeseks tipuks
       * @param vid loodava tipu nimi stringina
       * @return  loodud tipp Vetrex-tüüpi objektina
        */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid, (int) (Math.random()*100));
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Meetod kahe etteantud tipu vahel kaare loomiseks
       * @param aid loodava kaare nimi stringina
       * @param from kaare alguspunkt Vertex-tüüpi objektina
       * @param to kaare sihtpunkt Vertex-tüüpi objektina
        * @return kahe etteantud punkti vahel loodud kaar Arc-tüüpi objektina
        */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Meetod graafi esimese punkti määramiseks
       * @param first esimene punkt Vertex-tüüpi objektina
        */
      public void setFirstVertex(Vertex first) {
         this.first = first;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      } /*End createRandomTree*/

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      } /*End createAdjMatrix*/

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       * @author Jaanus Pöial
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         /**n-1 edges created here */
         createRandomTree (n);
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         /** remaining edges */
         int edgeCount = m - n + 1;
         while (edgeCount > 0) {
            /**random source*/
            int i = (int)(Math.random()*n);
            /**random target*/
            int j = (int)(Math.random()*n);
            if (i==j)
               /** no loops */
               continue;
            if (connected [i][j] != 0 || connected [j][i] != 0)
               /**no multiple edges*/
               continue;
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            /**a new edge happily created*/
            edgeCount--;
         }
      }/*End createRandomSimpleGraph*/

      /**
       * Põhimeetod, milles kutsutakse välja kõrgeima tipu leidmise meetodit ja
       * graafi laiuti läbimise meetodit.
       * @param a algtipp
       * @param b siht-tipp
       * @return tagastab Act-tüüpi objektina esimese kaare.
        */
      public Arc findWayThroughHighestPoint (Vertex a, Vertex b) {
         if (a == null || b == null || a.first == null && b.first == null) {
            throw new RuntimeException("One or both of the vertex are null or arcs are null");
         }
         Vertex x = findHighestPoint(a, b);
         /** Leian tee punktist a kõigeimasse punkti x*/
         Arc xa = findAWay(a, x);

         /** Leian tee kõrgeimast punktist x lõpp-punkti b*/
         Arc bx = findAWay(x, b);

         Arc next = null;

         /** Leian kõige esimese kaare*/
         Arc current = bx;
         do {
            next = new Arc(current.id, current.target, next);
            current = current.next;
         } while (current != null);

         current = xa;
         do {
            next = new Arc(current.id, current.target, next);
            current = current.next;
         } while (current != null);
         return next;
      } /*End findWayThroughHighestPoint*/

      /**
       * Meetod graafi tippude värvi "eemaldamiseks"
       */
      public void clearColors() {
         Vertex current = first;
         while (current != null) {
            current.setVColor(Color.White);
            current = current.next;
         }
      }/*End clearColors*/

       /**
        * Meetod graafi laiuti läbimiseks.
        * @param a algtipp
        * @param b siht-tipp
        * @return tagastab Arc-tüüpi objektina viimase kaare
        */
      public Arc findAWay(Vertex a, Vertex b) {
         if (a == null || b == null) {
            throw new RuntimeException("One or both of the vertex are null");
         }
         /**Puhastame tipud värvidest*/
         clearColors();

         /**List töötlemist ootavate tippude hoidmiseks*/
         LinkedList<Arc> que = new LinkedList();
                 Arc currentArc = null;
         Vertex currentPoint = a;
         /** Värvime töötlemise järjekorda lisatud tipu halliks */
         a.setVColor(Color.Grey);

         try {
            while (currentPoint != null) {
               if (currentPoint.getVColor() != Color.Black) {
                  /** Värvime punkti mustaks*/
                  currentPoint.setVColor(Color.Black);
                  /**Kaar tee määramiseks*/
                  Arc arc = currentPoint.getFirstArc();

                  /**While tsükkel punkti kõikide arkide läbimiseks*/
                  while (arc != null) {
                     if (arc.target.getVColor() != Color.Grey) {
                        arc.target.setVColor(Color.Grey);

                        Arc pathArc = new Arc(arc.id, arc.target, currentArc);
                        que.addLast(pathArc);

                        if (que.size() > 0 && pathArc.target == b) {
                           return que.getLast();
                        }
                     }
                     arc = arc.next;
                  }
               }
               /**Võtan järrgmise kaare järjekorrast*/
               currentArc = que.pop();
               currentPoint = currentArc != null
                       ? currentArc.target
                       : null;
            }/*End while*/
         } catch (RuntimeException e) {
            throw new RuntimeException("There is no path to connect vertex " + a + "with vertex " + b);
         }
         return null;
      } /*End findAWay*/

      /**
       * Meetod, mis otsib kahe etteantud punkti vahel kõrgeimat
       * võimalikku tippu (point).
       * @param a algtipp
       * @param b siht-tipp
       * @return tagastab kõrgeima tipu
       */
      public Vertex findHighestPoint(Vertex a, Vertex b) {
         if (a == null || b == null) {
            throw new RuntimeException("One or both of the vertex are null");
         }
         /**tipp kõrgeima väärtuse hoidmiseks*/
         Vertex highest = null;
         /**vaadeldav tipp*/
         Vertex point = this.first.next;

         while (point != null) {
            if (String.valueOf(point.h) == "") {
               throw new RuntimeException("One of the vertex height is empty");
            }
            /**
             * Lisan vaadeldava punkti listi, kui see on kõrgem kui vana väärtus
             * Arvesse ei tohi võtta alguspunkti a ja lõpp-punkti b väärtust
             */
            if(point != a && point != b && (highest == null || point.h > highest.h)) {
               highest = point;
            }
            point = point.next;
         }

         if (highest !=  null) {
            System.out.println("Highest point is " + highest);
            return highest;
         }
         throw new RuntimeException("There not enough vertexes for this task");
      } /*End findHighestPoint*/
      
   } /* End Graph*/

   /**
    * Erlaid klass stringi liitmiseks, kuna Moodle ei loeta Java8-t
    * @author Meetodi allikas: http://stackoverflow.com/questions/794248/a-method-to-reverse-effect-of-java-string-split/3629711#3629711
    */
   public static class StringUtils {
      /**
       * Meetod stringi liitmiseks
       * @param elements liidetavad objektid
       * @param separator liidetavate objektide vahele lisatav separator
       * @return objektid, mille vahele on pandud separator stringina
        */
      public static String join(Iterable<? extends Object> elements, CharSequence separator)
      {
         StringBuilder builder = new StringBuilder();

         if (elements != null)
         {
            Iterator<? extends Object> iter = elements.iterator();
            if(iter.hasNext())
            {
               builder.append( String.valueOf( iter.next() ) );
               while(iter.hasNext())
               {
                  builder
                          .append( separator )
                          .append( String.valueOf( iter.next() ) );
               }
            }
         }
         return builder.toString();
      }

   }/*End StringUtils*/

} /*End GraphTask*/

